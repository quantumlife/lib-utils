module bitbucket.org/sproutifyio/lib-utils

go 1.16

require (
	github.com/beevik/guid v0.0.0-20170504223318-d0ea8faecee0 // indirect
	github.com/fsnotify/fsnotify v1.4.9
	github.com/jinzhu/gorm v1.9.16
	github.com/mitchellh/mapstructure v1.1.2
	github.com/nats-io/nats-server/v2 v2.2.2 // indirect
	github.com/nats-io/nats.go v1.10.1-0.20210419223411-20527524c393
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/spf13/viper v1.7.1
	google.golang.org/protobuf v1.26.0 // indirect
)
